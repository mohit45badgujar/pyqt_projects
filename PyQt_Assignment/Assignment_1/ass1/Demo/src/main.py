# Create a basic PyQt application with a main window that includes a QLabel displaying
# the text "Hello, PyQt!" in window.
import sys
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout, QLabel
from PyQt5.QtCore import Qt

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("Main Window")

        self.setGeometry(500,200,500,300)

        self.mainLayout=QVBoxLayout(self)

        self.setLayout(self.mainLayout)

        self.LabelUI()

    def LabelUI(self):
        self.Label=QLabel("Hello PyQt")
        self.mainLayout.addWidget(self.Label,0,Qt.AlignmentFlag.AlignCenter)

if __name__=='__main__':
    app=QApplication(sys.argv)
    main_Window=MainWindow()
    main_Window.show()
    sys.exit(app.exec_())