import sys
from PyQt5.QtWidgets import QApplication,QLabel,QLineEdit, QPushButton,QVBoxLayout, QHBoxLayout, QFormLayout, QFileDialog, QRadioButton, QWidget,QMessageBox, QScrollArea, QSplitter
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
from django import db

class C2W_LoginPage(QWidget):

    def __init__(self):

        super().__init__()
        self.c2w_init_ui()

    def c2w_init_ui(self):
    
        header_label = QLabel('Logo')
        header_label.setAlignment(Qt.AlignCenter)
        #header_label.setStyleSheet('background-color: #003A6B; color: white;padding: 10px; font-size: 22px; max-height:40px')
        
        header_label = QLabel('Sign into your FREE account')
        header_label.setAlignment(Qt.AlignCenter)

        self.user_name = QLineEdit()
        self.password = QLineEdit()

        self.login_button = QPushButton('Login')

        self.output_label = QLabel()
        self.output_label.setStyleSheet('font-size: 14px; margin-top: 10px;')

        button_layout = QVBoxLayout()
        button_layout.addWidget(self.login_button)

        left_layout = QVBoxLayout()
        left_layout.addWidget(header_label)
        left_layout.addLayout(button_layout)
        left_layout.addWidget(self.output_label)

        self.records_widget = QWidget() # Widget to hold the records layout
        self.records_layout = QVBoxLayout(self.records_widget) # QVBoxLayout for the records
        self.records_layout.setAlignment(Qt.AlignTop) # Align records to the top

        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(self.records_widget)

        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.setSizes([self.width() // 2, self.width() // 2])
        splitter.setStyleSheet("QSplitter::handle {background: lightgray;}")
        splitter.widget(0).setLayout(left_layout)
        splitter.widget(1).setLayout(QVBoxLayout())
        splitter.widget(1).layout().addWidget(scroll_area)
        main_layout = QVBoxLayout()
        main_layout.addWidget(splitter)
        self.setLayout(main_layout)

        self.login_button.clicked.connect(self.login_click)

        user_name = self.user_name.text()
        password = self.password.text()

        output_text = f"Name: {user_name} {password}\n"

        user_login_ref = db.collection('login')

        user_profile = {
            'user_name': user_name,
            'password': password,
        }
    
    def login_click(self):
        pass