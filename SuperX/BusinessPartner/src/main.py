import sys
from PyQt5.QtWidgets import QApplication
from PyQt5 import QtGui

from Login.loginPage import C2W_LoginPage


if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=C2W_LoginPage()
    ex.setWindowIcon(QtGui.QIcon('pyqt_projects/SuperX/BusinessPartner/assets/profilr_logo.png'))
    ex.setWindowTitle('Login Page')
    ex.setGeometry(200,100,1000,600)
    ex.show()
    sys.exit(app.exec_())