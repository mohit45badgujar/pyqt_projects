import sys
from PyQt5 import QtGui
from PyQt5 import QtWidgets, QtCore

class AboutUsPage(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()
        self.setStyleSheet('background-color: #FFFFFF;')

    def init_ui(self):
        # Create central widget
        central_widget = QtWidgets.QWidget()
        layout = QtWidgets.QVBoxLayout(central_widget)
        self.setCentralWidget(central_widget)

        # Create horizontal layout for back button and "About Us" label
        header_layout = QtWidgets.QHBoxLayout()

        # Create back button with image
        back_button = QtWidgets.QPushButton()
        back_button.setIcon(QtGui.QIcon("pyqt_projects/SuperX/pract/demo/backbt.png"))  # Replace with actual image path
        back_button.setIconSize(QtCore.QSize(30, 30))  # Adjust icon size as needed
        back_button.setFixedWidth(30)
        back_button.setFixedHeight(30)
        header_layout.addWidget(back_button)

        # Add "About Us" label with centered alignment
        self.center_label = QtWidgets.QLabel("About Us")
        self.center_label.setFont(QtGui.QFont("Arial", 14))
        self.center_label.setStyleSheet("color: #333;margin-top: 0px; margin-bottom: 10px;padding: 0px; font-size: 30px;font-weight:bold; max-height:50px;text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5)")
        self.center_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        header_layout.addWidget(self.center_label)

        layout.addLayout(header_layout)

        # Create label for scrolling images
        self.image_label = QtWidgets.QLabel()
        self.image_index = 0  # Index to track the current image
        self.images = [
            "SuperX/pract/sir1.jpeg",
            "SuperX/pract/sir3.jpeg",
            "SuperX/pract/demo/mohit.jpeg",
            "SuperX/pract/contact.jpg",
            "SuperX/pract/contact1.py",
            "SuperX/pract/logo.png",
            "SuperX/pract/contact.jpg",
            "SuperX/pract/profile.jpg"
        ]
        self.image_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        # Create a timer to scroll images horizontally
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.scroll_image)
        self.timer.start(2000)  # Change image every 2 seconds

        layout.addWidget(self.image_label)

        # Partner cards area (implement if needed)
        # ... (your code for partner cards)

        client_title_layout = QtWidgets.QLabel("Our Members")
        client_title_layout.setFont(QtGui.QFont("Arial", 20))
        client_title_layout.setStyleSheet('font-weight:500 ; margin-top: 10px;')
        client_title_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(client_title_layout)

        # Create a horizontal layout for client cards
        client_cards_layout = QtWidgets.QHBoxLayout()

        # Define client data (replace with actual information)
        clients = [
            {'name': 'Member 1', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'BE ENTC'},
            {'name': 'Member 2', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'BE ENTC'},
            {'name': 'Member 3', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'BE ENTC'},
            {'name': 'Member 4', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'BE ENTC'},
            # Add more clients as needed
        ]

        # Create and format client cards dynamically
        for client in clients:
            card_widget = QtWidgets.QWidget()
            card_layout = QtWidgets.QVBoxLayout(card_widget)

            photo_label = QtWidgets.QLabel()
            photo_label.setPixmap(QtGui.QPixmap(client['logo']).scaled(80, 80, QtCore.Qt.KeepAspectRatio))
            photo_label.setScaledContents(True)
            photo_label.setStyleSheet('border-radius: 50%; background-color: #FFFFFF;')  # Rounded

            name_label = QtWidgets.QLabel(client['name'])
            name_label.setFont(QtGui.QFont('Arial', 16))
            name_label.setStyleSheet('font-weight:500 ; margin-top: 10px;')

            info_label = QtWidgets.QLabel(client['info'])
            info_label.setFont(QtGui.QFont('Arial', 10))
            info_label.setWordWrap(True)
            info_label.setStyleSheet('text-align: justify;font-weight:400')

            card_layout.addWidget(photo_label, alignment=QtCore.Qt.AlignCenter)
            card_layout.addWidget(name_label, alignment=QtCore.Qt.AlignCenter)
            card_layout.addWidget(info_label, alignment=QtCore.Qt.AlignCenter)

            client_cards_layout.addWidget(card_widget, alignment=QtCore.Qt.AlignCenter)

        # Add client cards layout to the main layout
        layout.addLayout(client_cards_layout)

        # Add vertical spacer for visual balance (optional)
        spacer_item = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        layout.addItem(spacer_item)

        self.setWindowTitle("About Us")
        self.show()

    def create_shadowed_label(self):
        shadow_style = """
            border: 1px solid black;
            background-color: rgba(0, 0, 0, 0.7);
            backdrop-filter: blur(10px);
            border-radius: 5px;
        """
        text_style = "color: white; padding: 20px;"
        label = QtWidgets.QLabel()
        label.setStyleSheet(shadow_style + text_style)
        return label

    def scroll_image(self):
        # Get current position
        current_pos = self.image_label.pos()
        x_pos = current_pos.x()

        # Scroll the image to the right
        x_pos -= 10
        if x_pos <= -self.image_label.width():
            x_pos = self.width()  # Restart from the right edge

        # Update position
        self.image_label.move(x_pos, current_pos.y())

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = AboutUsPage()
    window.setGeometry(20, 40, 1000, 700)  # Adjust window size as needed
    window.show()
    sys.exit(app.exec_())
