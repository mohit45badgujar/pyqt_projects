import sys
import webbrowser
from PyQt5 import QtCore, QtGui, QtWidgets

class ContactUsPage(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        # Create a scroll area
        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setWidgetResizable(True)

        # Create a widget to contain the main layout
        scroll_widget = QtWidgets.QWidget()
        scroll_area.setWidget(scroll_widget)

        # Main layout for better organization
        main_layout = QtWidgets.QVBoxLayout(scroll_widget)

        # Visual elements
        self.setStyleSheet("background-color: lightgrey;")
        self.add_logo(main_layout)
        self.add_company_info(main_layout)
        self.add_description(main_layout)
        self.add_team_members(main_layout)
        self.add_contact_info(main_layout)
        self.add_social_media_links(main_layout)
        self.add_contact_form()

        # Set layout for the scroll area
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(scroll_area)

        # Set window title and show the page
        self.setWindowTitle("Contact Us")
        self.show()

    def add_logo(self, main_layout):
        logo_label = QtWidgets.QLabel(self)
        logo_label.setPixmap(QtGui.QPixmap("clickandcollab/assets/Mohit/contact.jpg").scaled(1500, 250, QtCore.Qt.KeepAspectRatio))
        main_layout.addWidget(logo_label, alignment=QtCore.Qt.AlignCenter)

    def add_company_info(self, main_layout):
        company_name_label = QtWidgets.QLabel("Contact Us")
        company_name_label.setStyleSheet("font-size: 24px; font-weight: bold;")
        main_layout.addWidget(company_name_label, alignment=QtCore.Qt.AlignCenter)

        tagline_label = QtWidgets.QLabel("Fuel Your Startup...")
        tagline_label.setStyleSheet("font-size:24px;font-weight:bold;text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5);")
        main_layout.addWidget(tagline_label, alignment=QtCore.Qt.AlignCenter)

    def add_description(self, main_layout):
        contact_us_text = """
            Coming together is a beginning, Staying together is progress, and working together is success...
        """
        contact_us_label = QtWidgets.QLabel(contact_us_text)
        contact_us_label.setStyleSheet("font-size:18px; font-weight: bold")
        contact_us_label.setWordWrap(True)
        main_layout.addWidget(contact_us_label)

    def add_team_members(self, main_layout):
        team_members_label = QtWidgets.QLabel("<b>Our Team</b>")
        team_members_label.setStyleSheet("font-size:18px;font-weight: bold;")
        main_layout.addWidget(team_members_label)

        team_members_text = """
            - John Doe (CEO)
            - Jane Smith (CTO)
            - Michael Johnson (CFO)
            - Emily Brown (COO)
        """
        team_members_label = QtWidgets.QLabel(team_members_text)
        team_members_label.setWordWrap(True)
        team_members_label.setStyleSheet("font-size:16px;")
        main_layout.addWidget(team_members_label)

    def add_contact_info(self, main_layout):
        contact_label = QtWidgets.QLabel("<b>Contact Info</b>")
        contact_label.setStyleSheet("font-size:18px;font-weight: bold;")
        main_layout.addWidget(contact_label)

        contact_info_text = """
            - Email: clickandcollab123@gmail.com
            - Phone: 1234567890
            - Address: JSPM NTC, Dhule
        """
        contact_info_label = QtWidgets.QLabel(contact_info_text)
        contact_info_label.setWordWrap(True)
        contact_info_label.setStyleSheet("font-size:16px;")
        main_layout.addWidget(contact_info_label)

    def add_social_media_links(self, main_layout):
        social_media_label = QtWidgets.QLabel("<b>Social Media Links</b>")
        social_media_label.setStyleSheet("font-size:18px;font-weight: bold;")
        main_layout.addWidget(social_media_label)

        # Create buttons for social media links
        facebook_button = QtWidgets.QPushButton("Facebook")
        instagram_button = QtWidgets.QPushButton("Instagram")
        linkedin_button = QtWidgets.QPushButton("LinkedIn")

        # Connect buttons to their respective handlers
        facebook_button.clicked.connect(self.open_facebook)
        instagram_button.clicked.connect(self.open_instagram)
        linkedin_button.clicked.connect(self.open_linkedin)

        # Add buttons to layout
        social_media_layout = QtWidgets.QHBoxLayout()
        social_media_layout.addWidget(facebook_button)
        social_media_layout.addWidget(instagram_button)
        social_media_layout.addWidget(linkedin_button)
        main_layout.addLayout(social_media_layout)

    def open_facebook(self):
        url = "https://www.facebook.com/core2webtechnologies"
        webbrowser.open(url)

    def open_instagram(self):
        url = "https://www.instagram.com/core2web?igsh=MXJmOHdla3Q3bHc2NA=="
        webbrowser.open(url)

    def open_linkedin(self):
        url = "https://www.facebook.com/core2webtechnologies"
        webbrowser.open(url)

    def add_contact_form(self):
        # Add a QFormLayout with fields for name, email, message, etc.
        pass  # Implement this section as needed

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = ContactUsPage()
    window.setGeometry(20, 40, 1000, 700)
    sys.exit(app.exec_())

