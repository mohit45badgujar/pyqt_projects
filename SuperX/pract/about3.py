import sys
from PyQt5 import QtGui
from PyQt5 import QtWidgets, QtCore


class AboutUsPage(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()
        self.setStyleSheet('background-color: #FFFFFF;')

    def init_ui(self):
        # Create central widget
        central_widget = QtWidgets.QWidget()
        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(central_widget)
        self.setCentralWidget(scroll_area)

        layout = QtWidgets.QVBoxLayout(central_widget)

        # Create back button with image
        back_button = QtWidgets.QPushButton()
        back_button.setIcon(QtGui.QIcon("SuperX/pract/demo/backbt.png"))  # Replace with actual image path
        back_button.setIconSize(QtCore.QSize(40, 40))  # Adjust icon size as needed
        back_button.setFixedWidth(30)
        back_button.setFixedHeight(30)
        back_button.clicked.connect(self.go_back)

        # Add "About Us" label with centered alignment
        center_label = QtWidgets.QLabel("About Us")
        center_label.setFont(QtGui.QFont("Arial", 14))
        center_label.setStyleSheet(
            "color: #333;margin:5px;padding: 20px; font-size: 36px; max-height:50px;text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5)"
        )
        center_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        horizontal_layout = QtWidgets.QHBoxLayout()
        horizontal_layout.addWidget(back_button)
        horizontal_layout.addWidget(center_label)

        # Create horizontal layout for image and description
        sec_horizontal_layout = QtWidgets.QHBoxLayout()

        # Description text box (with shadow effect)
        description_text_box = self.create_shadowed_label()
        description_text_box.setText("""
                        Do You need a Partner for business ...?
                                     
           So we have brought for you Click and Collab... Where you can find your business partner as you want, with the skills you want, of your choice.
           On our Website you can find the business partner you want ...
                                     
                            LET'S START THE JOURNEY...
        """)
        description_text_box.setWordWrap(True)
        description_text_box.setStyleSheet(
            "font-size: 20px; margin-top: 0px; margin-bottom: 0px;margin-right:0px ;background-color: #000000;color:white"
        )

        # Image widget
        self.image_label = QtWidgets.QLabel()
        self.image_index = 0  # Index to track the current image
        self.images = [
            "SuperX/pract/aboutUs/about4.png",
            "SuperX/pract/aboutUs/about5.png",
            "SuperX/pract/aboutUs/about6.png",
            "SuperX/pract/aboutUs/about7.png",
            "SuperX/pract/aboutUs/about4.png",
            "SuperX/pract/aboutUs/about5.png",
            "SuperX/pract/aboutUs/about6.png",
            "SuperX/pract/aboutUs/about7.png"
        ]
        image_timer = QtCore.QTimer(self)
        image_timer.timeout.connect(self.change_image)
        image_timer.start(3000)  # Change image every 3 seconds
        self.change_image()  # Initial image

        sec_horizontal_layout.setAlignment(QtCore.Qt.AlignTop)
        sec_horizontal_layout.addWidget(description_text_box)
        sec_horizontal_layout.addWidget(self.image_label)
        layout.addLayout(horizontal_layout)
        layout.addLayout(sec_horizontal_layout)
        # Add padding to align elements at the top
        layout.addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))

        # Partner cards area (implement if needed)
        client_title_layout = QtWidgets.QHBoxLayout()
        client_title_layout.setAlignment(QtCore.Qt.AlignCenter)
        client_title_label = QtWidgets.QLabel("Our Members")
        client_title_label.setStyleSheet("font-size:25px;")
        client_title_label.setFont(QtGui.QFont("Arial", 20))
        client_title_layout.addWidget(client_title_label)
        layout.addLayout(client_title_layout)

        # Create a vertical layout for client cards
        client_cards_layout = QtWidgets.QHBoxLayout()

        # Define client data (replace with actual information)
        clients = [
            {'name': 'Member 1', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'Co-Founder'},
            {'name': 'Member 2', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'Co-Founder'},
            {'name': 'Member 3', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'Co-Founder'},
            {'name': 'Member 4', 'logo': 'SuperX/pract/demo/mohit.jpeg', 'info': 'Co-Founder'},
        ]

        # Create and format client cards dynamically
        for client in clients:
            card_widget = QtWidgets.QWidget()
            card_layout = QtWidgets.QVBoxLayout(card_widget)

            photo_label = QtWidgets.QLabel()
            photo_label.setPixmap(QtGui.QPixmap(client['logo']).scaled(100, 100, QtCore.Qt.KeepAspectRatio))
            photo_label.setScaledContents(True)
            photo_label.setStyleSheet('border-radius: 50%; background-color: #FFFFFF;')  # Rounded

            name_label = QtWidgets.QLabel(client['name'])
            name_label.setFont(QtGui.QFont('Arial', 16))
            name_label.setStyleSheet('font-weight:500 ; margin-top: 10px;')

            info_label = QtWidgets.QLabel(client['info'])
            info_label.setFont(QtGui.QFont('Arial', 11))
            info_label.setWordWrap(True)
            info_label.setStyleSheet('text-align: justify;font-weight:400')

            card_layout.addWidget(photo_label, alignment=QtCore.Qt.AlignCenter)
            card_layout.addWidget(name_label, alignment=QtCore.Qt.AlignCenter)
            card_layout.addWidget(info_label, alignment=QtCore.Qt.AlignCenter)

            client_cards_layout.addWidget(card_widget, alignment=QtCore.Qt.AlignCenter)

        # Add client cards layout to the main layout
        layout.addLayout(client_cards_layout)

        # Add vertical spacer for visual balance (optional)
        spacer_item = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        layout.addItem(spacer_item)

        self.setWindowTitle("About Us")
        self.show()

    def create_shadowed_label(self):
        shadow_style = """
            border: 1px solid black;
            background-color: rgba(0, 0, 0, 0.7);
            border-radius: 5px;
        """
        text_style = "color: white; padding: 20px;"
        label = QtWidgets.QLabel()
        label.setStyleSheet(shadow_style + text_style)
        return label

    def change_image(self):
        self.image_label.setPixmap(QtGui.QPixmap(self.images[self.image_index]).scaled(600, 310, QtCore.Qt.KeepAspectRatio))
        self.image_index = (self.image_index + 1) % len(self.images)

    def go_back(self):
        # Add functionality to go back to the previous page
        print("Back button clicked!")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = AboutUsPage()
    window.setGeometry(20, 40, 1000, 700)  # Adjust window size as needed
    window.show()
    sys.exit(app.exec_())
